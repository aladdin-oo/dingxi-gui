import requests
import pytest
import allure

@allure.step("新增宠物")
def test_create_pet():
    url = "https://petstore.swagger.io/v2/pet"
    headers = {"Content-Type": "application/json"}
    data = {
        "id": 1,
        "name": "Tom",
        "photoUrls": [],
        "status": "available"
    }
    response = requests.post(url, headers=headers, json=data)
    assert response.status_code == 200

@allure.step("查询宠物")
def test_find_pet_by_status():
    url = "https://petstore.swagger.io/v2/pet/findByStatus"
    params = {"status": "available"}
    response = requests.get(url, params=params)
    assert response.status_code == 200

